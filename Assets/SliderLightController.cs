using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderLightController : MonoBehaviour
{
    [SerializeField] Light light;

    Slider m_slider;
    // Start is called before the first frame update
    void Start()
    {
        m_slider = GetComponent<Slider>();
        m_slider.value = light.intensity / 2.5f;
    }

    public void OnValueChange()
    {
        light.intensity = m_slider.value * 2.5f;
    }
}
