using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace UnityEngine.XR.ARFoundation.Samples
{
    /// <summary>
    /// Listens for touch events and performs an AR raycast from the screen touch point.
    /// AR raycasts will only hit detected trackables like feature points and planes.
    ///
    /// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
    /// and moved to the hit position.
    /// </summary>
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceOnPlane : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Instantiates this prefab on a plane at the touch location.")]
        GameObject m_PlacedPrefab; //m_ballPrefab;

        [SerializeField]
        GameObject[] objs; int countObjs;
        List<GameObject> lstObj = new List<GameObject>();

        [SerializeField] Transform cam;

        /// <summary>
        /// The prefab to instantiate on touch.
        /// </summary>
        public GameObject placedPrefab
        {
            get { return m_PlacedPrefab; }
            set { m_PlacedPrefab = value; }
        }

        /// <summary>
        /// The object instantiated as a result of a successful raycast intersection with a plane.
        /// </summary>
        public GameObject spawnedObject { get; private set; }

        [SerializeField] GameObject allUI, stopScaneUI;
        [SerializeField] ARMeshManager arMeshManager;

        public bool isSaveMemory = false;

        bool isBarret = true;

        void Awake()
        {
            m_RaycastManager = GetComponent<ARRaycastManager>();
        }

        float widthScreen, heightScreen;

        [SerializeField] GameObject planeEditor;

        private void Start()
        {
            allUI.SetActive(false);
            widthScreen = Screen.width;
            heightScreen = Screen.height;
            prefabs = objs[0];
#if !UNITY_EDITOR
            Destroy(planeEditor);
#endif
        }

        public void OnClickStopScan()
        {
            arMeshManager.enabled = false;
            allUI.SetActive(true);
            stopScaneUI.SetActive(false);
        }

        public void OnClickStartScan()
        {
            arMeshManager.enabled = true;
            allUI.SetActive(false);
            stopScaneUI.SetActive(true);
        }

        bool TryGetTouchPosition(out Vector2 touchPosition)
        {
            if (Input.touchCount > 0)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }

            touchPosition = default;
            return false;
        }

        public void OnClickThrow()
        {
            //countObjs = countObjs < objs.Length ? countObjs : 0;
            GameObject obj;
            if (isSaveMemory)
            {
                obj = Instantiate(prefabs, cam.position, Quaternion.identity);
            }
            else
            {
                obj = CreateObj(prefabs);
            }

            obj.GetComponent<Rigidbody>().AddForce(cam.forward * 7.5f, ForceMode.VelocityChange);
            lstObj.Add(obj);
            //countObjs++;
        }


        public GameObject CreateObj(GameObject objPrefab)
        {
            GameObject obj = new GameObject();
            obj.transform.position = cam.position;
            obj.name = objPrefab.name + "(Clone)";
            obj.AddComponent<Rigidbody>();

            Rigidbody rig;
            rig = obj.GetComponent<Rigidbody>();
            rig.interpolation = RigidbodyInterpolation.Interpolate;
            rig.collisionDetectionMode = CollisionDetectionMode.Continuous;

            if (objPrefab.CompareTag("Barret"))
            {
                obj.transform.localScale = new Vector3(.08f, .08f, .08f);
                isBarret = true;
            }
            else
            {
                isBarret = false;
            }

            foreach (Transform _transform in objPrefab.transform)
            {
                if(_transform.gameObject.activeSelf)
                {
                    if (_transform.name != "Collider")
                    {
                        GameObject child = new GameObject();
                        Transform trans_Child = child.transform;
                        trans_Child.SetParent(obj.transform);
                        //Transform
                        trans_Child.name = _transform.name;
                        trans_Child.localPosition = _transform.localPosition;
                        trans_Child.localEulerAngles = _transform.localEulerAngles;
                        trans_Child.localScale = _transform.localScale;
                        //MeshRenderer
                        if (_transform.GetComponent<MeshRenderer>())
                        {
                            child.AddComponent<MeshRenderer>();
                            child.AddComponent<MeshFilter>();
                            child.GetComponent<MeshRenderer>().material = SetMaterial(_transform);
                            child.GetComponent<MeshFilter>().sharedMesh = _transform.GetComponent<MeshFilter>().sharedMesh;
                        }
                    }
                    else
                    {
                        GameObject collider = Instantiate(_transform, obj.transform).gameObject;
                        collider.transform.localPosition = _transform.localPosition;
                        collider.transform.localEulerAngles = _transform.localEulerAngles;
                        collider.transform.localScale = _transform.localScale;
                    }
                }                
            }
            return obj;
        }

        Material SetMaterial(Transform _transform)
        {
            Material mat = new Material(_transform.GetComponent<MeshRenderer>().sharedMaterial.shader);
            mat.name = "aaa";
            mat.CopyPropertiesFromMaterial(_transform.GetComponent<MeshRenderer>().sharedMaterial);

            setTexture2D("_BaseMap", mat);

            return mat;
        }

        void setTexture2D(string nameTexture, Material mat)
        {
            int textWidth, textHeight;
            Texture srcTexture = mat.GetTexture(nameTexture);
            textWidth = srcTexture.width;
            textHeight = srcTexture.height;

            Texture2D new2D = (Texture2D)srcTexture;
            TextureFormat textureFormat = new2D.format;

            if (isBarret)
            {
                Texture2D newTexture = new Texture2D(textWidth, textHeight, textureFormat, false);
                Graphics.CopyTexture(srcTexture, newTexture);
                mat.SetTexture(nameTexture, newTexture);
            }
            else
            {
                Texture2D newTexture = new Texture2D(textWidth, textHeight, textureFormat, true);
                Graphics.CopyTexture(srcTexture, newTexture);
                mat.SetTexture(nameTexture, newTexture);
            }



        }

        [SerializeField] GameObject light;
        [SerializeField] Text m_text;
        public void TurnLight()
        {
            light.SetActive(!light.active);
            m_text.text = light.active ? "Light On" : "Light Off";
        }

        [SerializeField] Text m_textSaveMemory;

        public void TurnSaveMemory()
        {
            Debug.Log("isSaveMemory: " + isSaveMemory);
            isSaveMemory = !isSaveMemory;
            m_textSaveMemory.text = isSaveMemory ? "Save Memory On" : "Save Memory Off";
        }

        GameObject prefabs;

        [SerializeField] Dropdown m_dropDown;
        public void OnOptionChange()
        {
            Debug.Log("m_dropDown.value: " + m_dropDown.value);
            switch (m_dropDown.value)
            {
                case 0:
                    prefabs = objs[0];
                    break;
                case 1:
                    prefabs = objs[1];
                    break;
                case 2:
                    prefabs = objs[2];
                    break;
                case 3:
                    prefabs = objs[3];
                    break;
                case 4:
                    prefabs = objs[4];
                    break;
                case 5:
                    prefabs = objs[5];
                    break;
                case 6:
                    prefabs = objs[6];
                    break;
                case 7:
                    prefabs = objs[7];
                    break;
                case 8:
                    prefabs = objs[8];
                    break;
                case 9:
                    prefabs = objs[9];
                    break;
                case 10:
                    prefabs = objs[10];
                    break;

            }
        }

        public void DelAll()
        {
            foreach (GameObject obj in lstObj)
            {
                Destroy(obj);
            }
        }

        private bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        void Update()
        {
            if (!IsPointerOverUIObject())
            {
                if (!stopScaneUI.active)
                {
#if !UNITY_EDITOR
                    if (!TryGetTouchPosition(out Vector2 touchPosition))
                        return;

                    if (Input.GetMouseButtonDown(0))
                    {
                        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.All))
                        {
                            // Raycast hits are sorted by distance, so the first one
                            // will be the closest hit.
                            var hitPose = s_Hits[0].pose;

                            //if (spawnedObject == null)
                            //{
                            //    spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                            //}
                            //else
                            //{
                            //countObjs = countObjs < objs.Length ? countObjs : 0;
                            GameObject obj;
                            if (isSaveMemory)
                            {
                                obj = Instantiate(prefabs, cam.position, Quaternion.identity);
                            }
                            else
                            {
                                //GameObject _obj = Resources.Load<GameObject>("Obj");
                                //obj = Instantiate((GameObject)_obj, cam.position, Quaternion.identity);
                                obj = CreateObj(prefabs);
                            }
                            obj.transform.position = hitPose.position;
                            Vector3 target = new Vector3(0, Camera.main.transform.position.y, 0);
                            obj.transform.LookAt(Camera.main.transform.position);
                            obj.transform.eulerAngles = new Vector3(0, obj.transform.eulerAngles.y, 0);
                            lstObj.Add(obj);
                            //Vector3 direction = hitPose - cam.position;
                            //obj.GetComponent<Rigidbody>().AddForce(direction.normalized * 15, ForceMode.VelocityChange);
                            //countObjs++;

                            //countObjs = countObjs < objs.Length ? countObjs : 0;
                            //GameObject obj = Instantiate(objs[countObjs], cam.position, Quaternion.identity);
                            //Vector3 direction = hitPose.position - cam.position;
                            //obj.GetComponent<Rigidbody>().AddForce(direction.normalized * 7.5f, ForceMode.VelocityChange);
                            //countObjs++;

                            //obj.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(hitPose.position.x, hitPose.position.y + 4, hitPose.position.z), hitPose.position, ForceMode.Impulse);
                            //}
                        }
                    }
#else
                    if (Input.GetMouseButtonDown(0))
                    {
                        RaycastHit hit;
                        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                        {
                            // Raycast hits are sorted by distance, so the first one
                            // will be the closest hit.
                            var hitPose = hit.point;

                            //if (spawnedObject == null)
                            //{
                            //    spawnedObject = Instantiate(m_PlacedPrefab, hitPose, Quaternion.identity);
                            //}
                            //else
                            //{
                            //countObjs = countObjs < objs.Length ? countObjs : 0;
                            GameObject obj = Instantiate(prefabs, hitPose, Quaternion.identity);
                            obj.transform.LookAt(Camera.main.transform.position);
                            obj.transform.eulerAngles = new Vector3(0, obj.transform.eulerAngles.y, 0);
                            lstObj.Add(obj);
                            //Vector3 direction = hitPose - cam.position;
                            //obj.GetComponent<Rigidbody>().AddForce(direction.normalized * 15, ForceMode.VelocityChange);
                            //countObjs++;
                            //obj.GetComponent<Rigidbody>().AddForce(hitPose, ForceMode.Impulse);
                            //obj.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(hitPose.x, hitPose.y + 1, hitPose.z * 2), hitPose, ForceMode.Impulse);
                            //}
                        }
                    }
#endif
                }
            }
        }

        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

        ARRaycastManager m_RaycastManager;
    }
}
