using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.Profiling;
using System.Threading;
using System;
using System.Diagnostics;
using System.Linq;


//-----------------------------------------------------------------------------------------------------
public class StatsMan : MonoBehaviour
{
    [SerializeField] private float updateInterval = 1;

    private Thread _cpuThread;

    private float _lasCpuUsage;

    public float CpuUsage;

    [SerializeField] Text m_textRam, m_textCpu;

    [Tooltip("The amount of physical CPU cores")]
    [SerializeField] private int processorCount;
    // Use this for initialization
    void Start()
    {
        // setup the thread
        _cpuThread = new Thread(UpdateCPUUsage)
        {
            IsBackground = true,
            // we don't want that our measurement thread
            // steals performance
            Priority = System.Threading.ThreadPriority.BelowNormal
        };

        // start the cpu usage thread
        _cpuThread.Start();
    }

    // Update is called once per frame
    void Update()
    {
        float ramPercent = (((float)Profiler.GetTotalAllocatedMemoryLong() / 1048576) / SystemInfo.systemMemorySize) * 100;
        m_textRam.text = "Ram Usage: " + System.Math.Round(ramPercent, 2) + "%";
        UnityEngine.Debug.Log("Profiler.GetTotalAllocatedMemoryLong: " + (((float)Profiler.GetTotalAllocatedMemoryLong() / 1048576) / SystemInfo.systemMemorySize) * 100 + "%");

        // for more efficiency skip if nothing has changed
        if (Mathf.Approximately(_lasCpuUsage, CpuUsage)) return;

        // the first two values will always be "wrong"
        // until _lastCpuTime is initialized correctly
        // so simply ignore values that are out of the possible range
        if (CpuUsage < 0 || CpuUsage > 100) return;

        // I used a float instead of int for the % so use the ToString you like for displaying it
        m_textCpu.text = "Cpu Usage: " + System.Math.Round(CpuUsage, 2).ToString() + "%";
        UnityEngine.Debug.Log("CpuUsage: " + CpuUsage.ToString() + "%");
        //cpuCounterText.text = CpuUsage.ToString("F1") + "% CPU";

        // Update the value of _lasCpuUsage
        _lasCpuUsage = CpuUsage;
    }

    private void UpdateCPUUsage()
    {
        var lastCpuTime = new TimeSpan(0);

        // This is ok since this is executed in a background thread
        while (true)
        {
            var cpuTime = new TimeSpan(0);

            // Get a list of all running processes in this PC
            var AllProcesses = Process.GetProcesses();

            // Sum up the total processor time of all running processes
            cpuTime = AllProcesses.Aggregate(cpuTime, (current, process) => current + process.TotalProcessorTime);

            // get the difference between the total sum of processor times
            // and the last time we called this
            var newCPUTime = cpuTime - lastCpuTime;

            // update the value of _lastCpuTime
            lastCpuTime = cpuTime;

            // The value we look for is the difference, so the processor time all processes together used
            // since the last time we called this divided by the time we waited
            // Then since the performance was optionally spread equally over all physical CPUs
            // we also divide by the physical CPU count
            CpuUsage = 100f * (float)newCPUTime.TotalSeconds / updateInterval / processorCount;

            // Wait for UpdateInterval
            Thread.Sleep(Mathf.RoundToInt(updateInterval * 1000));
        }
    }

    private void OnDestroy()
    {
        // Just to be sure kill the thread if this object is destroyed
        _cpuThread?.Abort();
    }

    private void OnValidate()
    {
        // We want only the physical cores but usually
        // this returns the twice as many virtual core count
        //
        // if this returns a wrong value for you comment this method out
        // and set the value manually
        processorCount = SystemInfo.processorCount / 2;
    }
}
