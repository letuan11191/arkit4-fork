﻿using UnityEngine;
using System.Collections;
using UnityEngine.Profiling;

public class FPSDisplay : MonoBehaviour
{
	float deltaTime = 0.0f;

    private void Start()
    {
		Application.targetFrameRate = 60;
    }

    void Update()
	{
		deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
	}

	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;

		GUIStyle style = new GUIStyle();

		Rect rect = new Rect(0, 250, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperRight;
		style.fontSize = h * 2 / 100;
		//style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
		style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
		GUI.Label(rect, text, style);

		Rect rect2 = new Rect(0, 300, w, h * 2 / 100);
		float ramPercent = (((float)Profiler.GetTotalAllocatedMemoryLong() / 1048576) / SystemInfo.systemMemorySize) * 100;
		//string textRam = "Ram Usage: " + System.Math.Round(ramPercent, 2) + "%";
		string textRam = "Ram Usage: " + ((float)Profiler.GetTotalAllocatedMemoryLong() / 1048576).ToString("00") + " MB";
		GUI.Label(rect2, textRam, style);

	}
}
