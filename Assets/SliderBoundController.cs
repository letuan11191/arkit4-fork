using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBoundController : MonoBehaviour
{
    Slider m_slider;
    [SerializeField] Text m_text;
    [SerializeField] PhysicMaterial bound;
    // Start is called before the first frame update
    void Start()
    {
        m_slider = GetComponent<Slider>();
        m_slider.value = bound.bounciness;
        //m_text.text = "Bounciness: " + m_slider.value;
    }

    public void OnValueChange()
    {
        bound.bounciness = m_slider.value;
        //m_text.text = "Bound: " + m_slider.value ;
    }
}
