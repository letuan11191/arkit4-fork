using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanningAnim : MonoBehaviour
{
    Text m_text;
    // Start is called before the first frame update
    void Start()
    {
        m_text = GetComponent<Text>();
        StartCoroutine(textAnim());
    }

    IEnumerator textAnim()
    {
        yield return new WaitForSeconds(.25f);
        if(m_text.text.Length < 12)
        {
            m_text.text += ".";
        }
        else
        {
            m_text.text = "Scanning";
        }
        StartCoroutine(textAnim());
    }
}
